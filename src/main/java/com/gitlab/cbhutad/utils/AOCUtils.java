package com.gitlab.cbhutad.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AOCUtils {
    
    public static List<String> readFile(String path) {
        List<String> fileEntries = new ArrayList<String>();
        
        try {
            File file = new File(path);
            Scanner sc = new Scanner(file);
            while(sc.hasNextLine()) {
                fileEntries.add(sc.nextLine());
            }
            sc.close();
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
        return fileEntries;
    }
}
