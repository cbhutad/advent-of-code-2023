package com.gitlab.cbhutad.problems;

import java.util.ArrayList;
import java.util.List;

import com.gitlab.cbhutad.utils.AOCUtils;

public class Second {
    
    private String path;
    private static final int redcubes = 12;
    private static final int greencubes = 13;
    private static final int bluecubes = 14;

    /**
     * Default constructor
     */
    public Second(String path) {
        this.path = path;
    }

    /**
     * To calculate the sum of games which have a valid sets drawn for part one
     * @return int
     */
    public int calculateSumPartOne() {

        int sum = 0;
        boolean validSet;

        List<List<String>> setEntries = filterInput();
        for(List<String> entry : setEntries) {

            validSet = true;

            for(int i = 1;i < entry.size();i++) {
                if(validateSetDrawn(entry.get(i)))
                    continue;
                else {
                    validSet = false;
                    break;
                }
            }
            if(validSet) {
                String[] partitions = entry.get(0).split(" ");
                sum += Integer.parseInt(partitions[1]);
            }
        }

        return sum;
    }

    /**
     * to validate the sets drawn
     * @return boolean
     */
    private boolean validateSetDrawn(String set) {
        String[] partitions = set.split(",");
        for(String entry : partitions) {
            
            entry = entry.trim();

            if(entry.contains("green")) {
                int greencubesDrawn = Integer.parseInt(entry.split(" ")[0]);
                if (!(greencubes >= greencubesDrawn && greencubesDrawn >= 0))
                    return false;
            }

            if(entry.contains("blue")) {
                int bluecubesDrawn = Integer.parseInt(entry.split(" ")[0]);
                if (!(bluecubes >= bluecubesDrawn && bluecubesDrawn >= 0))
                    return false;
            }

            if(entry.contains("red")) {
                int redcubesDrawn = Integer.parseInt(entry.split(" ")[0]);
                if (!(redcubes >= redcubesDrawn && redcubesDrawn >= 0))
                    return false;
            }
        }
        return true;
    }

    /**
     * Filtering the input
     * @return List<List<String>>
     */
    private List<List<String>> filterInput() {
        List<List<String>> setEntries = new ArrayList<>();
        List<String> fileEntries = AOCUtils.readFile(path);

        for(String entry : fileEntries) {
            String[] partitions = entry.split(":");
            List<String> temp = new ArrayList<>();
            temp.add(partitions[0]);
            for(String str : partitions[1].split(";"))
                temp.add(str);
            setEntries.add(temp);                
        }
        return setEntries;
    }

    /**
     * To calculate the sum of games which have a valid sets drawn for part two
     * @return int
     */
    public int calculateSumPartTwo() {

        int sum = 0;

        List<List<String>> setEntries = filterInput();
        for(List<String> entry : setEntries) {

            int powerValueForEntry = calculatePowerValue(entry);
            sum += powerValueForEntry;
        }

        return sum;
    }

    /**
     * To calculate the power value for each entry
     * @return int
     */
    private int calculatePowerValue(List<String> entry) {
        
        int greenMax = -1, redMax = -1, blueMax = -1;

        for(int i = 1;i < entry.size(); i++) {
            
            String[] partitions = entry.get(i).split(",");
            for(String str : partitions) {
                
                if(str.contains("green")) {
                    if(greenMax < Integer.parseInt(str.trim().split(" ")[0]))
                        greenMax = Integer.parseInt(str.trim().split(" ")[0]);
                }

                if(str.contains("red")) {
                    if(redMax < Integer.parseInt(str.trim().split(" ")[0]))
                        redMax = Integer.parseInt(str.trim().split(" ")[0]);
                }

                if(str.contains("blue")) {
                    if(blueMax < Integer.parseInt(str.trim().split(" ")[0]))
                        blueMax = Integer.parseInt(str.trim().split(" ")[0]);
                }

            }
        }

        return greenMax * blueMax * redMax;

    }

}
