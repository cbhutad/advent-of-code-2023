package com.gitlab.cbhutad.problems;

import java.util.List;

import com.gitlab.cbhutad.utils.AOCUtils;

public class First {
    
    private String path;

    public First(String path){
        this.path = path;
    }

    /**
     * To calculate the sum required by problem for part one
     * @return int
     */
    public int calculateSumPartOne() {
        int first = -1, last = -1, sum = 0;
        List<String> entries = AOCUtils.readFile(path);
        for(String entry : entries) {
            for(int i = 0;i < entry.length();i++) {
                if(entry.charAt(i) - '0' <= 9 && '9' - entry.charAt(i) >= 0) {
                    if (first == -1)
                        first = entry.charAt(i) - '0';
                    last = entry.charAt(i) - '0';
                }
            }
            sum += first * 10 + last;
            first = -1;
            last = -1;
        }
        return sum;
    }

    /**
     * To calculate the sum required by problem for part two
     * @return int
     */
    public int calculateSumPartTwo() {

        int minPos, maxPos, minVal = -1, maxVal = -1, tempminPos, tempmaxPos, sum = 0;
        List<String> entries = AOCUtils.readFile(path);
        for(String entry : entries) {

            minPos = entry.length();
            maxPos = -1;

            tempminPos = entry.indexOf("0");
            tempmaxPos = entry.lastIndexOf("0");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 0;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 0;
            }
            
            tempminPos = entry.indexOf("1");
            tempmaxPos = entry.lastIndexOf("1");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 1;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 1;
            }

            tempminPos = entry.indexOf("2");
            tempmaxPos = entry.lastIndexOf("2");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 2;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 2;
            }

            tempminPos = entry.indexOf("3");
            tempmaxPos = entry.lastIndexOf("3");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 3;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 3;
            }
            
            tempminPos = entry.indexOf("4");
            tempmaxPos = entry.lastIndexOf("4");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 4;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 4;
            }

            tempminPos = entry.indexOf("5");
            tempmaxPos = entry.lastIndexOf("5");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 5;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 5;
            }

            tempminPos = entry.indexOf("6");
            tempmaxPos = entry.lastIndexOf("6");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 6;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 6;
            }
            
            tempminPos = entry.indexOf("7");
            tempmaxPos = entry.lastIndexOf("7");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 7;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 7;
            }

            tempminPos = entry.indexOf("8");
            tempmaxPos = entry.lastIndexOf("8");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 8;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 8;
            }

            tempminPos = entry.indexOf("9");
            tempmaxPos = entry.lastIndexOf("9");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 9;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 9;
            }            
            
            tempminPos = entry.indexOf("one");
            tempmaxPos = entry.lastIndexOf("one");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 1;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 1;
            }
        
            tempminPos = entry.indexOf("two");
            tempmaxPos = entry.lastIndexOf("two");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 2;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 2;
            }            

            tempminPos = entry.indexOf("three");
            tempmaxPos = entry.lastIndexOf("three");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 3;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 3;
            }            

            tempminPos = entry.indexOf("four");
            tempmaxPos = entry.lastIndexOf("four");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 4;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 4;
            }
        
            tempminPos = entry.indexOf("five");
            tempmaxPos = entry.lastIndexOf("five");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 5;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 5;
            }            

            tempminPos = entry.indexOf("six");
            tempmaxPos = entry.lastIndexOf("six");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 6;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 6;
            }            

            tempminPos = entry.indexOf("seven");
            tempmaxPos = entry.lastIndexOf("seven");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 7;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 7;
            }
        
            tempminPos = entry.indexOf("eight");
            tempmaxPos = entry.lastIndexOf("eight");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 8;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 8;
            }            

            tempminPos = entry.indexOf("nine");
            tempmaxPos = entry.lastIndexOf("nine");

            if(tempminPos < minPos && tempminPos != -1) {
                minPos = tempminPos;
                minVal = 9;
            }
            if(tempmaxPos > maxPos && tempmaxPos != -1) {
                maxPos = tempmaxPos;
                maxVal = 9;
            }            

            sum += minVal * 10 + maxVal;

        }

        return sum;
    }

}
