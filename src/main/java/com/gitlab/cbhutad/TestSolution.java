package com.gitlab.cbhutad;

import java.io.IOException;

import com.gitlab.cbhutad.problems.First;
import com.gitlab.cbhutad.problems.Second;

/**
 * Hello world!
 *
 */
public class TestSolution {

    public static void main( String[] args ) throws IOException {
        

        // Problem 1 test files
        String firstProblemFilePath = "/home/cbhutad/Work/advent-of-code-2023/src/main/resources/com/gitlab/cbhutad/1.txt";

        //Problem 2 test files
        String secondProblemFilePath = "/home/cbhutad/Work/advent-of-code-2023/src/main/resources/com/gitlab/cbhutad/2.txt";

        First problem = new First(firstProblemFilePath);
        //Second problem = new Second(secondProblemFilePath);
        
        System.out.println(problem.calculateSumPartOne());
        System.out.println(problem.calculateSumPartTwo());
    }
}
